
import methods from './methods';
import api from './api';
import * as general from './general';

export const API = api;
export const METHODS = methods;
export const GENERAL = general;
