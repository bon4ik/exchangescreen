// import { GENERAL } from './';

export default {
  BASE_URL: 'https://api.exchangeratesapi.io/',
  LATEST_RATES: 'latest?base=',

  getLatestRateUrl: function(base: string) {
    return `${this.LATEST_RATES}${base}`;
  },
};
