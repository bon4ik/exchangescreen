import api from './api';

describe('api', () => {
  describe('getLatestRateUrl', () => {
    it('should return url', () => {
      // given
      const givenBase = 'base';
      const expectedResult = `${api.LATEST_RATES}${givenBase}`;

      // when
      const result = api.getLatestRateUrl(givenBase);

      // then
      expect(result).toEqual(expectedResult);
    });
  });
});
