import { isCashStringValid } from 'src/utils';

describe('isCashStringValid', () => {
  describe('when argument consists letter', () => {
    it('should return false', () => {
      // given
      const givenArgument = 'a3';
    
      // when
      const result = isCashStringValid(givenArgument);
    
      // then
      expect(result).toBeFalsy();
    });
  });

  describe('when argument consists symbol', () => {
    it('should return false', () => {
      // given
      const givenArgument = '.3';
    
      // when
      const result = isCashStringValid(givenArgument);
    
      // then
      expect(result).toBeFalsy();
    });
  });

  describe('when argument fractional portion is more the 2 numbers', () => {
    it('should return false', () => {
      // given
      const givenArgument = '0.333';
    
      // when
      const result = isCashStringValid(givenArgument);
    
      // then
      expect(result).toBeFalsy();
    });
  });

  describe('when argument is number', () => {
    it('should return true', () => {
      // given
      const givenArgument = '1';
    
      // when
      const result = isCashStringValid(givenArgument);
    
      // then
      expect(result).toBeTruthy();
    });
  });

  describe('when argument fractional portion is less then 3', () => {
    it('should return true', () => {
      // given
      const givenArgument = '1.22';
    
      // when
      const result = isCashStringValid(givenArgument);
    
      // then
      expect(result).toBeTruthy();
    });
  });
});
