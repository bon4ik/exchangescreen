import { formatCurrency } from 'src/utils';

describe('formatCurrency', () => {
  describe('when . does not exist', () => {
    it('should return the same number', () => {
      // given
      const givenArgument = 31;
      const expectedResult = '31';
    
      // when
      const result = formatCurrency(givenArgument);
    
      // then
      expect(result).toEqual(expectedResult);
    });
  });

  describe('when . exists', () => {
    it('should return the number with sliced fractional portion', () => {
      // given
      const givenArgument = 31.777;
      const expectedResult = '31.77';
    
      // when
      const result = formatCurrency(givenArgument);
    
      // then
      expect(result).toEqual(expectedResult);
    });
  });
});
