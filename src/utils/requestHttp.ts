import axios from 'axios';
import { get as _get } from 'lodash';

import { API } from 'src/constants';

const instance = axios.create({
  baseURL: API.BASE_URL,
  headers: {
    'Content-Type': 'application/json'
  }
});

instance.interceptors.response.use(
  (res) => {
    return _get(res, 'data', res);
  },
  (error) => {
    return Promise.reject({
      extended: _get(error, 'response.data.error.extended', {}),
      message: _get(error, 'response.data.error.message', error),
      code: _get(error, 'response.data.error.errcode', 0),
    });
  }
);

export default instance;
