export const isCashStringValid = (value: string): boolean => /^\d+(?:\.\d{0,2})?$/g.test(value);
