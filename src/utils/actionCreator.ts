import { createAction } from 'redux-act';
/**
 * Creates action object
 *
 * @param  {string} moduleName A name of the module
 *
 * @return {function} A function that receives action name as a string and returns actions
 */

interface IActions {
  cancel: any;
  failure: any;
  request: any;
  success: any;
  start: any;
}

export const createAsyncAction = (type: string): IActions => ({
  cancel: createAction(type + '::CANCEL'),
  failure: createAction(type + '::FAILURE'),
  request: createAction(type + '::REQUEST'),
  success: createAction(type + '::SUCCESS'),
  start: createAction(type + '::START'),
});

export default (prefix: string) => (type: string) => createAsyncAction(`${prefix}:${type}`);
