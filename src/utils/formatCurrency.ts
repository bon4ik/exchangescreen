export const formatCurrency = (n: number): string => {
    const strN = '' + n;
    
    if (strN.indexOf('.') === -1) return strN;

    return strN.slice(0, strN.indexOf('.') + 3);
}