import store from 'src/store';

import { showNotification } from 'src/store/actions/notificationActions';
import { INotificationType } from 'src/types/notificationTypes';


export class Notification {
  static success = (message: string) => {
    store.dispatch(
      showNotification.request({
        message,
        status: INotificationType.SUCCESS
      })
    );
  }

  static error = (message: string) => {
    store.dispatch(
      showNotification.request({
        message,
        status: INotificationType.ERROR
      })
    );
  }
}
