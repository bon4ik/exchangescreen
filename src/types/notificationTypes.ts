export enum INotificationType {
  SUCCESS = 'SUCCESS',
  ERROR = 'ERROR',
}

export interface INotificationState {
  message: string | null;
  status: INotificationType | null;
}
