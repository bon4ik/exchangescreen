export interface IRate {
  [key: string]: number;
}

export interface IFXRatesState {
  rates: IRate | {};
  balances: IRate | {};
  currentBase: string;
  exchangeableBase: string;
  currentCurrencyValue: string;
  exchangeableCurrencyValue: string;
  executeDisabled: boolean;
}
