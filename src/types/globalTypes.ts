import { INotificationState, IFXRatesState } from '.';

export interface IState {
    notification: INotificationState;
    fxRates: IFXRatesState;
}