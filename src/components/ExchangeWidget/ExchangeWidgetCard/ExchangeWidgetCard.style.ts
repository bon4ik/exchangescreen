import styled from 'styled-components';
import { Card, Select } from 'antd';

export const ExchangeWidgetCardContainer = styled(Card)`
  width: 100%;
  height: 200px;

  .ant-card-body {
      width: 100%;
      height: 100%;
      display: flex;
      justify-content: space-between;
      align-items: center;
      flex-direction: row;
  }
`;

export const CurrencyContainer = styled.div`
  width: 100%;
`;

export const InputContainer = styled.div`
  width: 100%;
`;

export const CurrencySelect = styled(Select)`
  width: 100px;
`;

export const AvailableBalance = styled.p`
  color: ${({ isBalanceExceeds }: { isBalanceExceeds: boolean }) => (isBalanceExceeds ? 'red' : 'grey')};
  margin: 0;
`;


export const ExpectedBalance = styled.p`
  margin: 0;
  text-align: right;
`;
