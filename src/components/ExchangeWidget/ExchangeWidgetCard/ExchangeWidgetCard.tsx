import React from 'react';
import { SelectValue } from 'antd/lib/select';

import {
  ExchangeWidgetCardContainer,
  CurrencySelect,
  CurrencyContainer,
  AvailableBalance,
  InputContainer,
  ExpectedBalance,
} from './ExchangeWidgetCard.style';
import { NumericInput } from 'src/components/shared';

import { Select } from 'antd';
const { Option } = Select;

export interface IExchangeWidgetCardProps {
  inputValue: string;
  onInputChange: (value: string) => void;
  onCurrencyChange: (value: SelectValue) => void;
  defaultCurrency: string;
  balance: number;
  isPositive: boolean;
  options: string[];
  executeDisabled?: boolean;
}

const ExchangeWidgetCard: React.FC<IExchangeWidgetCardProps> = ({
  inputValue,
  onInputChange,
  isPositive,
  options,
  defaultCurrency,
  onCurrencyChange,
  balance,
  executeDisabled,
}) => (
  <ExchangeWidgetCardContainer>
    <CurrencyContainer>
      <CurrencySelect
        bordered={false}
        showSearch
        optionFilterProp="children"
        onChange={onCurrencyChange}
        value={defaultCurrency}
        filterOption={(input, option) => option!.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
      >
        {options.map((option: string) => <Option key={option} value={option}>{option}</Option>)}
      </CurrencySelect>
      <AvailableBalance isBalanceExceeds={executeDisabled!}>Balance: {balance}</AvailableBalance>
    </CurrencyContainer>
    <InputContainer>
      <NumericInput value={inputValue} onChange={onInputChange} isPositive={isPositive} />
      {executeDisabled && <ExpectedBalance>exceeds balance</ExpectedBalance>}
    </InputContainer>
  </ExchangeWidgetCardContainer>
);

export default ExchangeWidgetCard;
