import React from 'react';
import { mount } from 'enzyme';

import ExchangeWidgetCard, { IExchangeWidgetCardProps } from 'src/components/ExchangeWidget/ExchangeWidgetCard/ExchangeWidgetCard';
import { ExpectedBalance } from 'src/components/ExchangeWidget/ExchangeWidgetCard/ExchangeWidgetCard.style';

describe('ExchangeWidgetCard Component', () => {
  const getComponnet = (props: IExchangeWidgetCardProps) => mount(<ExchangeWidgetCard {...props} />);

  describe('when executeDisabled is false', () => {
    it('should not render ExpectedBalance', () => {
      // given
      const givenProps = {
        inputValue: 'value',
        onInputChange: jest.fn(),
        onCurrencyChange: jest.fn(),
        defaultCurrency: 'default',
        balance: 1,
        isPositive: true,
        options: ['one', 'two'],
        executeDisabled: false,
      };
      const component = getComponnet(givenProps);
      
      // then
      expect(component.find(ExpectedBalance).length).toEqual(0);
    });
  });

  describe('when executeDisabled is true', () => {
    it('should render ExpectedBalance', () => {
      // given
      const givenProps = {
        inputValue: 'value',
        onInputChange: jest.fn(),
        onCurrencyChange: jest.fn(),
        defaultCurrency: 'default',
        balance: 1,
        isPositive: true,
        options: ['one', 'two'],
        executeDisabled: true,
      };
      const component = getComponnet(givenProps);
      
      // then
      expect(component.find(ExpectedBalance).length).toEqual(1);
    });
  });
});