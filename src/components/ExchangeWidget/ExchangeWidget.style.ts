import styled from 'styled-components';
import { Button as AntdButton } from 'antd';

export const ExchangeWidgetMain = styled.div``;

export const ExchangeWidgetFooter = styled.div``;

export const ExchangeWidgetContainer = styled.div`
  max-width: 400px;
  width: 100%;
  background: #fff;
  padding-bottom: 20px;
  margin: 20px auto;
  position: relative;
  box-shadow: 0px 3px 5px 0px rgba(0,0,0,0.55);
`;

export const Button = styled(AntdButton)`
  margin: 20px auto 0 auto;
  display: block;
`;

export const SwapButton = styled(AntdButton)`
  position: absolute;
  z-index: 1;
  top: 184px;
  left: 35px;
  transform: rotate(90deg);
`;

export const ChartButton = styled(AntdButton)`
  margin: 0 auto;
  border-radius: 25px;
  display: block;
  position: absolute;
  left: 0;
  right: 0;
  top: 184px;
`;
