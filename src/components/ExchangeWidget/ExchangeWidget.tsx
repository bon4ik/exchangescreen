import React, { useCallback, useEffect, useMemo } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { RetweetOutlined } from '@ant-design/icons';
import { SelectValue } from 'antd/lib/select';
import numeral from 'numeral';

import {
  fetchFXRatesActions,
  setCurrentBaseActions,
  setExchangeableBaseActions,
  setBalanceActions,
  setExchangeableCurrencyValue,
  setCurrentCurrencyValue,
  setExecuteDisabled,
} from 'src/store/actions/fxRatesActions';
import {
  getRates,
  getBalances,
  getRatesCurrencies,
  getBalancesCurrencies,
  getCurrentBase,
  getExchangeableBase,
  getExchangeableCurrencyValue,
  getCurrentCurrencyValue,
  getExecuteDisabled,
} from 'src/store/selectors';
import {
  ExchangeWidgetContainer,
  ExchangeWidgetMain,
  ChartButton,
  SwapButton,
  ExchangeWidgetFooter,
  Button,
} from './ExchangeWidget.style';
import ExchangeWidgetCard from './ExchangeWidgetCard';
import { formatCurrency } from 'src/utils';

const ExchangeWidget: React.FC = () => {
  const dispatch = useDispatch();
  const rates = useSelector(getRates);
  const balances = useSelector(getBalances);
  const ratesCurrencies = useSelector(getRatesCurrencies);
  const balancesCurrencies = useSelector(getBalancesCurrencies);
  const currentBase = useSelector(getCurrentBase);
  const exchangeableBase = useSelector(getExchangeableBase);
  const exchangeableCurrencyValue = useSelector(getExchangeableCurrencyValue);
  const executeDisabled = useSelector(getExecuteDisabled);
  const currentCurrencyValue = useSelector(getCurrentCurrencyValue);  

  const onChangeExchangeableCurrencyCallback = useCallback(
    (value: string) => {
      if (parseFloat(value)) {
        const currentValue = formatCurrency(numeral(value)
                                              .divide(rates[exchangeableBase])
                                              .value());

        dispatch(setCurrentCurrencyValue.success(currentValue));
        dispatch(setExecuteDisabled.success(parseFloat(currentValue) > balances[currentBase]));
      } else {
        dispatch(setCurrentCurrencyValue.success(value));
        dispatch(setExecuteDisabled.success(true));
      }

      dispatch(setExchangeableCurrencyValue.success(value));
    },
    [dispatch, balances, currentBase, rates, exchangeableBase]
  );

  const onChangeCurrentCurrencyCallback = useCallback(
    (value: string) => {
      if (parseFloat(value)) {
        const exchangeableValue = formatCurrency(numeral(value)
                                                  .multiply(rates[exchangeableBase])
                                                  .value());

        dispatch(setExchangeableCurrencyValue.success(exchangeableValue));
        dispatch(setExecuteDisabled.success(parseFloat(value) > balances[currentBase]));
      } else {
        dispatch(setExchangeableCurrencyValue.success(value));
        dispatch(setExecuteDisabled.success(true));
      }

      dispatch(setCurrentCurrencyValue.success(value));
    },
    [dispatch, balances, currentBase, rates, exchangeableBase]
  );

  const onChangeCurrentBase = useCallback(
    (value: SelectValue) => dispatch(setCurrentBaseActions.success(value)),
    [dispatch]
  );

  const onChangeExchangeableBase = useCallback(
    (value: SelectValue) => {
      dispatch(setExchangeableBaseActions.success(value));

      if (parseFloat(exchangeableCurrencyValue)) {
        const exchangeableValue = formatCurrency(numeral(currentCurrencyValue)
                                                  .multiply(rates[value as string])
                                                  .value());

        dispatch(setExchangeableCurrencyValue.success(exchangeableValue));
      } else {
        dispatch(setExchangeableCurrencyValue.success(exchangeableCurrencyValue));
      }
    },
    [dispatch, exchangeableCurrencyValue, currentCurrencyValue, rates]
  );

  const executeExchange = useCallback(
    () => {
      const currentValue = numeral(balances[currentBase]).subtract(currentCurrencyValue).value();
      const exchangeableValue = numeral(balances[exchangeableBase]).add(exchangeableCurrencyValue).value();

      dispatch(setBalanceActions.success({
        [exchangeableBase]: exchangeableValue,
        [currentBase]: currentValue,
      }));
      dispatch(setExecuteDisabled.success(!parseFloat(currentCurrencyValue) || parseFloat(currentCurrencyValue) > currentValue));
    },
    [dispatch, exchangeableCurrencyValue, exchangeableBase, currentBase, balances, currentCurrencyValue]
  );

  const swapCurrencies = useCallback(
    () => {
      dispatch(setCurrentBaseActions.success(exchangeableBase));
      dispatch(setExchangeableBaseActions.success(currentBase));
    },
    [dispatch, currentBase, exchangeableBase]
  ); 

  const fetchFXRates = useCallback(
    () => dispatch(fetchFXRatesActions.request()),
    [dispatch]
  );

  useEffect(() => {
    fetchFXRates();
  }, [fetchFXRates, currentBase]);

  const chartText = useMemo(() => `${currentBase} 1 = ${exchangeableBase} ${numeral(rates[exchangeableBase]).format('0,0.0000')}`,
  [currentBase, exchangeableBase, rates]);

  return (
    <ExchangeWidgetContainer>
      <ExchangeWidgetMain>
        <ExchangeWidgetCard
          inputValue={currentCurrencyValue}
          onInputChange={onChangeCurrentCurrencyCallback}
          options={balancesCurrencies}
          isPositive={false}
          onCurrencyChange={onChangeCurrentBase}
          defaultCurrency={currentBase}
          balance={balances[currentBase] || 0}
          executeDisabled={executeDisabled && !!parseFloat(currentCurrencyValue)}
        />
        <ExchangeWidgetCard
          inputValue={exchangeableCurrencyValue}
          onInputChange={onChangeExchangeableCurrencyCallback}
          options={ratesCurrencies}
          onCurrencyChange={onChangeExchangeableBase}
          defaultCurrency={exchangeableBase}
          balance={balances[exchangeableBase] || 0}
          isPositive
          executeDisabled={false}
        />
      </ExchangeWidgetMain>
      <ExchangeWidgetFooter>
        <Button type="primary" onClick={executeExchange} disabled={executeDisabled}>Execute</Button>
      </ExchangeWidgetFooter>
      <ChartButton onClick={() => alert('show chart')}>{chartText}</ChartButton>
      <SwapButton shape="circle" icon={<RetweetOutlined />} onClick={swapCurrencies} />
    </ExchangeWidgetContainer>
  );
}

export default ExchangeWidget;
