import React from 'react';
import { mount, ReactWrapper } from 'enzyme';
import { Provider } from 'react-redux';
import { Button } from 'antd';

import store from 'src/store';
import ExchangeWidget from './ExchangeWidget';
import ExchangeWidgetCard from './ExchangeWidgetCard';

const renderWithReduxProvider = (jsx: React.ReactElement): ReactWrapper => mount(
  <Provider store={ store }>
    { jsx }
  </Provider>
);

describe('ExchangeWidget Component', () => {
  let wrapper: ReactWrapper;

  beforeEach(() => {
    wrapper = renderWithReduxProvider(<ExchangeWidget />);
  });

  it('render', () => {
    expect(wrapper.find(ExchangeWidgetCard)).toHaveLength(2);
    expect(wrapper.find(Button)).toHaveLength(3);
  });
});
