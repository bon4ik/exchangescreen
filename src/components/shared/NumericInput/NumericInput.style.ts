import styled from 'styled-components';

import { Input as AntdInput } from 'antd';

export const Input = styled(AntdInput)`
  border: none;
  height: 100%;
  padding: 0;
  box-shadow: none;
  text-align: right;

  &:focus {
    box-shadow: none;
    border: none;
  }
`;
