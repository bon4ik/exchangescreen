import React from 'react';
import { mount } from 'enzyme';
import { Input } from 'antd';

import NumericInput, { INumericInputProps } from 'src/components/shared/NumericInput/NumericInput';

describe('NumericInput Component', () => {
  const getComponnet = (props: INumericInputProps) => mount(<NumericInput {...props} />);

  describe('when value is empty', () => {
    it('should render component with empty Input value', () => {
      // given
      const givenProps = {
        onChange: jest.fn(),
        value: '',
        isPositive: false,
      };
      const component = getComponnet(givenProps);
    
      // then
      expect(component.find(Input).length).toEqual(1);
      expect(component.props().value).toEqual('');
    });
  });

  describe('when value is not empty', () => {
    it('should render component with Input value', () => {
      // given
      const givenProps = {
        onChange: jest.fn(),
        value: '31',
        isPositive: false,
      };
      const component = getComponnet(givenProps);
    
      // then
      expect(component.find(Input).length).toEqual(1);
      expect(component.props().value).toEqual('31');
    });
  });

  describe('onInputChange', () => {
    describe('when value is not valid', () => {
      it('onChange should not be called', () => {
        // given
        const givenProps = {
          onChange: jest.fn(),
          value: '',
          isPositive: false,
        };
        const component = getComponnet(givenProps);

        // when
        component.find('input').simulate('change', { target: { value: 'wrong input' } });
        
        // then
        expect(givenProps.onChange).not.toHaveBeenCalled();
      });
    });

    describe('when value is not empty and valid', () => {
        it('onChange should be called', () => {
          // given
          const givenProps = {
            onChange: jest.fn(),
            value: '',
            isPositive: false,
          };
          const component = getComponnet(givenProps);
  
          // when
          component.find('input').simulate('change', { target: { value: '31' } });
          
          // then
          expect(givenProps.onChange).toHaveBeenCalled();
        });
      });
  });
});