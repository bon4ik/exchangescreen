import React, { useCallback } from 'react';

import { isCashStringValid } from 'src/utils';
import { Input } from './NumericInput.style';

export interface INumericInputProps {
  onChange: (value: string) => void;
  value: string;
  isPositive: boolean;
};

const NumericInput: React.FC<INumericInputProps> = ({ onChange, value, isPositive }) => {
  const onInputChange = useCallback(
    ({ target: { value } }: React.ChangeEvent<HTMLInputElement>) => {
      // do slice from 2 position because 0 and 1 for symbol + or -
      const valueWithoutSymbol = value[0] === '+' || value[0] === '-' ? value.slice(2) : value;
 
      if (isCashStringValid(valueWithoutSymbol) || valueWithoutSymbol === '') {
        onChange(valueWithoutSymbol);
      }
    },
    [onChange],
  );

  const inputValue = value ? `${isPositive ? '+' : '-'} ${value}` : '';

  return (
    <Input
      placeholder="0"
      value={inputValue}
      onChange={onInputChange}
    />
  );
}

export default NumericInput;
