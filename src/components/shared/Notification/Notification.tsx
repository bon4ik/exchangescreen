import React from 'react';
import { useSelector } from 'react-redux';

import { NotificationContainer } from './Notification.style';
import { getNotificationMessage } from 'src/store/selectors';

const Notification: React.FC = () => {
  const message = useSelector(getNotificationMessage);

  if (!message) {
      return null;
  }

  return <NotificationContainer message={message} type="error" />
}

export default Notification;
