import styled from 'styled-components';

import { Alert } from 'antd';

export const NotificationContainer = styled(Alert)`
  position: fixed;
  top: 20px;
  right: 20px;
`;