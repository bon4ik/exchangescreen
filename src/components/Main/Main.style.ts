import styled from 'styled-components';
import { Button as AntdButton } from 'antd';

export const Headline = styled.h1`
  width: 100%;
  text-align: center;
`;

export const MainContainer = styled.div`
  padding: 20px;
`;

export const Button = styled(AntdButton)`
  width: 158px;
  display: block;
  margin: 0 auto;
`;
