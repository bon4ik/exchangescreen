import React, { useState, Suspense, lazy } from 'react';

import {
  Headline,
  MainContainer,
  Button,
} from './Main.style';
import { Notification } from 'src/components/shared';

const ExchangeWidget = lazy(() => import('src/components/ExchangeWidget'));

const Main = () => {
  const [isExchangeWidgetOpen, setExchangeWidgetOpen] = useState(false);

  return (
    <MainContainer>
      <Headline>Main page</Headline>
      <Button type="primary" onClick={() => setExchangeWidgetOpen(!isExchangeWidgetOpen)}>Exchange Currency</Button>

      {isExchangeWidgetOpen && (
        <Suspense fallback={<div>Loading...</div>}>
          <ExchangeWidget />
        </Suspense>
      )}
      <Notification />
    </MainContainer>
  );
}

export default Main;
