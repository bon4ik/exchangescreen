import React from 'react';
import { mount, ReactWrapper } from 'enzyme';
import { Provider } from 'react-redux';
import { Button } from 'antd';

import store from 'src/store';
import Main from 'src/components/Main';

const renderWithReduxProvider = (jsx: React.ReactElement): ReactWrapper => mount(
  <Provider store={ store }>
    { jsx }
  </Provider>
);

describe('Main Component', () => {
  let wrapper: ReactWrapper;

  beforeEach(() => {
    wrapper = renderWithReduxProvider(<Main />);
  });

  it('should render component', () => {
    
    expect(wrapper.find('h1')).toHaveLength(1);
    expect(wrapper.find(Button)).toHaveLength(1);
  });
});
