import * as request from 'src/utils/requestHttp';
import { API } from 'src/constants';

import FXRatesService from './FXRatesService';

describe('FXRatesService', () => {
  describe('fetchFakeData', () => {
    it('should call request.get with parameters', () => {
        // given
        (request as any).default.get = jest.fn();
        const givenBase = 'base';

        // when
        FXRatesService.fetchFakeData(givenBase);

        // then
        expect(request.default.get).toBeCalledWith(API.getLatestRateUrl(givenBase));

    });
  });
});