import request from 'src/utils/requestHttp';
import { API } from 'src/constants'

export default {
  fetchFakeData: (base: string) => request.get(API.getLatestRateUrl(base)),
};
