import {
  getExchangeableBase,
  getCurrentBase,
  getBalances,
  getRates,
  getBalancesCurrencies,
  getRatesCurrencies,
  getExchangeableCurrencyValue,
  getExecuteDisabled,
  getCurrentCurrencyValue,
} from 'src/store/selectors';

describe('fxRatesSelectors', () => {
  describe('getExchangeableBase', () => {
    it('should return exchangeableBase from store', () => {
      // given
      const givenState = {
        fxRates: {
          rates: {},
          balances: {},
          currentBase: '',
          exchangeableBase: 'USD',
          exchangeableCurrencyValue: '',
          currentCurrencyValue: '',
          executeDisabled: true,
        },
        notification: {
          message: null,
          status: null,
        },
      };
      const expectedResult = 'USD';
      
      // when
      const result = getExchangeableBase(givenState);
      
      // then
      expect(result).toEqual(expectedResult);
    });
  });

  describe('getCurrentBase', () => {
    it('should return getCurrentBase from store', () => {
      // given
      const givenState = {
        fxRates: {
          rates: {},
          balances: {},
          currentBase: 'USD',
          exchangeableBase: '',
          exchangeableCurrencyValue: '',
          currentCurrencyValue: '',
          executeDisabled: true,
        },
        notification: {
          message: null,
          status: null,
        },
      };
      const expectedResult = 'USD';
      
      // when
      const result = getCurrentBase(givenState);
      
      // then
      expect(result).toEqual(expectedResult);
    });
  });

  describe('getBalances', () => {
    it('should return getBalances from store', () => {
      // given
      const expectedResult = {
        USD: 31,
      };
      const givenState = {
        fxRates: {
          rates: {},
          balances: expectedResult,
          currentBase: '',
          exchangeableBase: '',
          exchangeableCurrencyValue: '',
          currentCurrencyValue: '',
          executeDisabled: true,
        },
        notification: {
          message: null,
          status: null,
        },
      };
      
      // when
      const result = getBalances(givenState);
      
      // then
      expect(result).toEqual(expectedResult);
    });
  });

  describe('getRates', () => {
    it('should return getRates from store', () => {
      // given
      const expectedResult = {
        USD: 31,
      };
      const givenState = {
        fxRates: {
          rates: expectedResult,
          balances: {},
          currentBase: '',
          exchangeableBase: '',
          exchangeableCurrencyValue: '',
          currentCurrencyValue: '',
          executeDisabled: true,
        },
        notification: {
          message: null,
          status: null,
        },
      };
      
      // when
      const result = getRates(givenState);
      
      // then
      expect(result).toEqual(expectedResult);
    });
  });

  describe('getBalancesCurrencies', () => {
    it('should return getBalancesCurrencies from store', () => {
      // given
      const givenState = {
        fxRates: {
          rates: {},
          balances: {
            USD: 31,
            AUD: 31,
            PLN: 31,
          },
          currentBase: 'USD',
          exchangeableBase: 'PLN',
          exchangeableCurrencyValue: '',
          currentCurrencyValue: '',
          executeDisabled: true,
        },
        notification: {
          message: null,
          status: null,
        },
      };
      const expectedResult = ['AUD'];
      
      // when
      const result = getBalancesCurrencies(givenState);
      
      // then
      expect(result).toEqual(expectedResult);
    });
  });

  describe('getRatesCurrencies', () => {
    it('should return getRatesCurrencies from store', () => {
      // given
      const givenState = {
        fxRates: {
          rates: {
            USD: 31,
            AUD: 31,
            PLN: 31,
          },
          balances: {},
          currentBase: 'USD',
          exchangeableBase: 'PLN',
          exchangeableCurrencyValue: '',
          currentCurrencyValue: '',
          executeDisabled: true,
        },
        notification: {
          message: null,
          status: null,
        },
      };
      const expectedResult = ['AUD'];
      
      // when
      const result = getRatesCurrencies(givenState);
      
      // then
      expect(result).toEqual(expectedResult);
    });
  });

  describe('getExchangeableCurrencyValue', () => {
    it('should return getExchangeableCurrencyValue from store', () => {
      // given
      const givenState = {
        fxRates: {
          rates: {
            USD: 31,
            AUD: 31,
            PLN: 31,
          },
          balances: {},
          currentBase: '',
          exchangeableBase: '',
          exchangeableCurrencyValue: 'value',
          currentCurrencyValue: '',
          executeDisabled: true,
        },
        notification: {
          message: null,
          status: null,
        },
      };
      const expectedResult = 'value';
      
      // when
      const result = getExchangeableCurrencyValue(givenState);
      
      // then
      expect(result).toEqual(expectedResult);
    });
  });

  describe('getExecuteDisabled', () => {
    it('should return getExecuteDisabled from store', () => {
      // given
      const givenState = {
        fxRates: {
          rates: {
            USD: 31,
            AUD: 31,
            PLN: 31,
          },
          balances: {},
          currentBase: '',
          exchangeableBase: '',
          exchangeableCurrencyValue: '',
          currentCurrencyValue: '',
          executeDisabled: true,
        },
        notification: {
          message: null,
          status: null,
        },
      };
      
      // when
      const result = getExecuteDisabled(givenState);
      
      // then
      expect(result).toBeTruthy();
    });
  });

  describe('getCurrentCurrencyValue', () => {
    it('should return getCurrentCurrencyValue from store', () => {
      // given
      const givenState = {
        fxRates: {
          rates: {
            USD: 31,
            AUD: 31,
            PLN: 31,
          },
          balances: {},
          currentBase: '',
          exchangeableBase: '',
          exchangeableCurrencyValue: '',
          currentCurrencyValue: 'value',
          executeDisabled: true,
        },
        notification: {
          message: null,
          status: null,
        },
      };
      const expectedResult = 'value';
      
      // when
      const result = getCurrentCurrencyValue(givenState);
      
      // then
      expect(result).toEqual(expectedResult);
    });
  });
});
