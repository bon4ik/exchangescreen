import {
  getNotificationMessage,
  getNotificationStatus,
} from 'src/store/selectors';
import { INotificationType } from 'src/types';

describe('notificationSelector', () => {
  describe('getNotificationMessage', () => {
    it('should return notification message from store', () => {
      // given
      const givenState = {
        fxRates: {
          rates: {},
          balances: {},
          currentBase: '',
          exchangeableBase: '',
        },
        notification: {
          message: 'message',
          status: null,
        },
      };
      const expectedResult = 'message';
      
      // when
      const result = getNotificationMessage(givenState);
      
      // then
      expect(result).toEqual(expectedResult);
    });
  });

  describe('getNotificationStatus', () => {
    it('should return notification message from store', () => {
      // given
      const givenState = {
        fxRates: {
          rates: {},
          balances: {},
          currentBase: '',
          exchangeableBase: '',
        },
        notification: {
          message: null,
          status: INotificationType.ERROR,
        },
      };
      
      // when
      const result = getNotificationStatus(givenState);
      
      // then
      expect(result).toEqual(INotificationType.ERROR);
    });
  });
});
