import { createSelector } from 'reselect';

import { IState } from 'src/types';

const selectState = (state: IState) => state.notification;

export const getNotificationMessage = createSelector(
  selectState,
  state => state.message
);

export const getNotificationStatus = createSelector(
  selectState,
  state => state.status
);
