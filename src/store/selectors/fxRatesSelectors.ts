import { createSelector } from 'reselect';

import { IState } from 'src/types';

const selectState = (state: IState) => state.fxRates;

export const getRatesCurrencies = createSelector(
  selectState,
  ({ rates, exchangeableBase, currentBase }) => {
    const filteredRates = [];

    for (let key in rates) {
      if (key !== exchangeableBase && key !== currentBase) {
        filteredRates.push(key);
      }
    }

    return filteredRates;
  },
);

export const getBalancesCurrencies = createSelector(
  selectState,
  ({ balances, exchangeableBase, currentBase }) => {
    const filteredBalances = [];

    for (let key in balances) {
      if (key !== exchangeableBase && key !== currentBase) {
        filteredBalances.push(key);
      }
    }

    return filteredBalances;
  },
);

export const getRates = createSelector(
  selectState,
  ({ rates }) => rates,
);

export const getBalances = createSelector(
  selectState,
  ({ balances }) => balances,
);


export const getCurrentBase = createSelector(
  selectState,
  ({ currentBase }) => currentBase,
);

export const getExchangeableBase = createSelector(
  selectState,
  ({ exchangeableBase }) => exchangeableBase,
);

export const getExchangeableCurrencyValue = createSelector(
  selectState,
  ({ exchangeableCurrencyValue }) => exchangeableCurrencyValue,
);

export const getCurrentCurrencyValue = createSelector(
  selectState,
  ({ currentCurrencyValue }) => currentCurrencyValue,
);

export const getExecuteDisabled = createSelector(
  selectState,
  ({ executeDisabled }) => executeDisabled,
);
