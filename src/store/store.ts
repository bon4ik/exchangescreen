import { createStore, applyMiddleware, compose } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';

import rootReducer from './reducers';
import appSagas from './sagas';

const sagaMiddleware = createSagaMiddleware();
const middlewares = [sagaMiddleware];
let composer: any = compose;

if (process.env.NODE_ENV === 'development') {
  composer = composeWithDevTools;
}

const store = createStore(
  rootReducer,
  undefined,
  composer(
    applyMiddleware(...middlewares)
  )
);

sagaMiddleware.run(appSagas);

export default store;
