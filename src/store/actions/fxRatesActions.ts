import { actionCreator } from 'src/utils';

export const MODULE_NAME = 'FX_RATES';
export const FETCH_FX_RATES = 'FETCH_FX_RATES';
export const SET_BALANCE = 'SET_BALANCE';
export const SET_CURRENT_BASE = 'SET_CURRENT_BASE';
export const SET_EXCHANGEABLE_BASE = 'SET_EXCHANGEABLE_BASE';
export const SET_EXCHANGEABLE_CURRENCY_VALUE = 'SET_EXCHANGEABLE_CURRENCY_VALUE';
export const SET_CURRENT_CURRENCY_VALUE = 'SET_CURRENT_CURRENCY_VALUE';
export const SET_EXECUTE_DISABLED = 'SET_EXECUTE_DISABLED';

const action = actionCreator(MODULE_NAME);

export const fetchFXRatesActions = action(FETCH_FX_RATES);
export const setBalanceActions = action(SET_BALANCE);
export const setCurrentBaseActions = action(SET_CURRENT_BASE);
export const setExchangeableBaseActions = action(SET_EXCHANGEABLE_BASE);
export const setCurrentCurrencyValue = action(SET_CURRENT_CURRENCY_VALUE);
export const setExchangeableCurrencyValue = action(SET_EXCHANGEABLE_CURRENCY_VALUE);
export const setExecuteDisabled = action(SET_EXECUTE_DISABLED);