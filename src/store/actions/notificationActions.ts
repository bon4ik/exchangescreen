import { actionCreator } from 'src/utils';

export const MODULE_NAME = 'NOTIFICATION';

export const SHOW_NOTIFICATION = 'SHOW_NOTIFICATION';
export const HIDE_NOTIFICATION = 'HIDE_NOTIFICATION';

const action = actionCreator(MODULE_NAME);

export const showNotification = action(SHOW_NOTIFICATION);
export const hideNotification = action(HIDE_NOTIFICATION);
