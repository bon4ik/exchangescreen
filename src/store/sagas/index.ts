import { all, fork } from 'redux-saga/effects';

import notificationSagas from './notificationSaga';
import fxRatesSaga from './fxRatesSaga';

const allSagas = [
  notificationSagas,
  fxRatesSaga,
];

export default function* appSagas() {
  yield all(
    allSagas.map(fork as any)
  );
}
