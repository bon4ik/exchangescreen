import { call, put, takeLatest, select, delay } from 'redux-saga/effects';
import numeral from 'numeral';

import { TIME_DELAY_FOR_FETCH } from 'src/constants/general';
import { 
  fetchFXRatesActions,
  setExchangeableCurrencyValue,
  setExecuteDisabled,
} from 'src/store/actions/fxRatesActions';
import { showNotification } from 'src/store/actions/notificationActions';
import {
  getCurrentBase,
  getExchangeableCurrencyValue,
  getCurrentCurrencyValue,
  getExchangeableBase,
  getBalances,
} from 'src/store/selectors';
import fxRatesService from 'src/services/FXRatesService';
import { formatCurrency } from 'src/utils';
import { INotificationType } from 'src/types';

export function* fetchFxRatesPeriodicallySaga() {
  while (true) {
	yield call(fetchFxRatesSaga);
	yield delay(TIME_DELAY_FOR_FETCH);
  }
}

export function* fetchFxRatesSaga() {
  try {
	const currentBase = yield select(getCurrentBase);
	const exchangeableCurrencyValue = yield select(getExchangeableCurrencyValue);
	const currentCurrencyValue = yield select(getCurrentCurrencyValue);  
	const exchangeableBase = yield select(getExchangeableBase);
	const balances = yield select(getBalances);

	const data = yield call(fxRatesService.fetchFakeData, currentBase);

	// add current rate
	data.rates[currentBase] = 1;

	yield put(fetchFXRatesActions.success(data.rates));

	if (parseFloat(exchangeableCurrencyValue)) {
	  const exchangeableValue = formatCurrency(numeral(currentCurrencyValue)
												.multiply(data.rates[exchangeableBase])
												.value());

	  yield put(setExchangeableCurrencyValue.success(exchangeableValue));
	  yield put(setExecuteDisabled.success(currentCurrencyValue > balances[currentBase]));
	}
  } catch (error) {
	yield put(fetchFXRatesActions.failure(error));
	yield put(showNotification.request({
	  message: error.message.message,
	  status: INotificationType.ERROR
	})); 
  }
}

export function* fxRatesSagas() {
  yield takeLatest(fetchFXRatesActions.request, fetchFxRatesPeriodicallySaga);
}

export default fxRatesSagas;