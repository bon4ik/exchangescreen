import { takeLatest, put, delay } from 'redux-saga/effects';

import { INotificationState } from 'src/types';
import { showNotification, hideNotification } from 'src/store/actions/notificationActions';


interface IPayload {
  payload: INotificationState;
}

export function* showNotificationSaga({ payload }: IPayload) {
  yield put(showNotification.success({
    message: payload.message,
    status: payload.status
  }));

  yield delay(3000);
  yield put(hideNotification.request());
}

export function* hideNotificationSaga() {
  yield put(hideNotification.success());
}

export function* notificationSagas() {
  yield takeLatest(showNotification.request, showNotificationSaga);
  yield takeLatest(hideNotification.request, hideNotificationSaga);
}

export default notificationSagas;
