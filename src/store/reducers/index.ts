import { combineReducers } from 'redux';

import fxRates from './fxRatesReducer';
import notification from './notificationReducer';

export default combineReducers({
  fxRates,
  notification,
});
