import { createReducer } from 'redux-act';
import produce from 'immer';

import {
  fetchFXRatesActions,
  setBalanceActions,
  setCurrentBaseActions,
  setExchangeableBaseActions,
  setCurrentCurrencyValue,
  setExchangeableCurrencyValue,
  setExecuteDisabled,
} from '../actions/fxRatesActions';

import { IRate, IFXRatesState } from 'src/types';
import { DEFAULT_CURRENT_BASE, DEFAULT_EXCHANGEABLE_BASE } from 'src/constants/general';

const initialState: IFXRatesState = {
  rates: {},
  balances: {
	USD: 372.21, // mocked data
	AUD: 0, // mocked data
  },
  currentBase: DEFAULT_CURRENT_BASE,
  exchangeableBase: DEFAULT_EXCHANGEABLE_BASE,
  exchangeableCurrencyValue: '',
  currentCurrencyValue: '',
  executeDisabled: true,
};

export default createReducer(
  {
	[fetchFXRatesActions.success]: (state, payload: IRate) =>
	  produce(state, nextState => {
		nextState.rates = payload;
	  }),
	[fetchFXRatesActions.failure]: state =>
	  produce(state, nextState => {
        nextState.rates = {};
	  }),
	[setBalanceActions.success]: (state, payload: IRate) =>
	  produce(state, nextState => {
        nextState.balances = { ...state.balances, ...payload };
	  }),
	[setCurrentBaseActions.success]: (state, payload: string) =>
	  produce(state, nextState => {
        nextState.currentBase = payload;
	  }),
	[setExchangeableBaseActions.success]: (state, payload: string) =>
	  produce(state, nextState => {
        nextState.exchangeableBase = payload;
	  }),
	[setCurrentCurrencyValue.success]: (state, payload: string) =>
	  produce(state, nextState => {
        nextState.currentCurrencyValue = payload;
	  }),
	[setExchangeableCurrencyValue.success]: (state, payload: string) =>
	  produce(state, nextState => {
        nextState.exchangeableCurrencyValue = payload;
	  }),
	[setExecuteDisabled.success]: (state, payload: boolean) =>
	  produce(state, nextState => {
        nextState.executeDisabled = payload;
	  }),
  },
  initialState,
);
