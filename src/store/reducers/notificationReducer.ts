import { createReducer } from 'redux-act';
import produce from 'immer';

import { INotificationState } from 'src/types';

import { showNotification, hideNotification } from '../actions/notificationActions';

const initialState: INotificationState = {
  message: null,
  status: null,
};

export default createReducer(
  {
	[showNotification.success]: (state, { message, status }: INotificationState) =>
	  produce(state, nextState => {
		nextState.message = message;
		nextState.status = status;
      }),
    [hideNotification.success]: (state) =>
	  produce(state, nextState => {
		nextState.message = null;
		nextState.status = null;
	  }),
  },
  initialState
);
